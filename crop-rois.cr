#!/usr/bin/env crystal
#

require "./lib/image.cr"

help = <<-help 
#
# crop-rois
#
# Este programa se encarga de recortar una región de las imágenes en "pnm", 
# escribe nuevas imágenes con las regiones recortadas.
#
# Uso: 
#   ls [im-dir]/* | ./select-rois.rb - -n=1 | ./crop-rois.rb -
#   ./crop-rois.rb - < [lista-regiones.txt] 
# 
# Opciones: 
#   --out-dir=         ruta donde colocar el directorio de imágenes recortadas
#   --dir-name=        directorio donde se colocan las imágenes recortadas
#   --verbose    | -v
#   --help       | -h 
#
# Entrada: 
#   Lee de STDIN los nombres de los archivos con orden lexicográfico y 
#   las regiones de interés de las cuales generar las nuevas imágenes
#   
#   Formato: 
#     # Comentario 
#     => Nombre-archivo-1.pgm
#     x1 y1 x2 y2
#     => Nombre-archivo-2.pgm 
#     x1 y1 x2 y2
#
#
# Salida: 
#   Escribe a STDOUT los nombres de los archivos generados
#   recorte-archivo-1.pgm 
#   recorte-archivo-2.pgm 
#
help

# Config  ---------------------------------------------------------------------

verbose    = false
out_dir = "out"
dir_name   = "crop"

# Parse config ----------------------------------------------------------------

if ARGV.empty? || ARGV.any?{|a| a =~ /^--help$|^-h$/} || !ARGV.includes?("-") 
  puts help 
  exit 0
end 

if k = ARGV.index{|a| a =~ /--out-dir=?/}
  out_dir = (ARGV[k] =~ /=/) ? ARGV[k].split(/=/)[-1] : ARGV[k+1]
end 

if k = ARGV.index{|a| a =~ /--dir-name=?/}
  dir_name = (ARGV[k] =~ /=/) ? ARGV[k].split(/=/)[-1] : ARGV[k+1]
end 

if k = ARGV.index{|a| a =~ /(--verbose|-v)/}
  verbose = true 
end 


# Programa =====================================================================


# Se asume que se pasa mediante STDIN el nombre de cada imagen a procesar y 
# las regiones de interés. 

puts "# Reading data" if verbose 

img_rois   = [] of Array(Array(Int32))
file_names = [] of String
rois       = [] of Array(Int32)
STDIN.each_line do |line|
  next if line =~ /(^\s*#.*$|^\s*$)/
  if line =~ /^\s*=>/
    filename = line.split(/=>\s*/)[1].strip
    file_names.push  filename   
    unless rois.empty?
      img_rois.push rois
    end 
    rois = [] of Array(Int32)
  else 
    rois.push line.strip.split(/\s+/).map{|e| e.to_f.to_i} 
  end 
end 
img_rois.push rois unless rois.empty?

exit 0 if rois.empty? 

sorted_indexes = (0 ... file_names.size).to_a.sort_by{|i| file_names[i]}
img_rois       = sorted_indexes.map{|i| img_rois[i]}
file_names     = sorted_indexes.map{|i| file_names[i]}

dir = "#{out_dir}/#{dir_name}"
Dir.mkdir_p(dir) 

nrois = img_rois[0].size 
(0 ... file_names.size).each do |i| 
  puts "# Loading image #{File.basename file_names[i]}"  if verbose
  im = Image.load(file_names[i])

  puts "# Crop image #{"%03d" % i}" if verbose 
  rois = img_rois[i]

  (0 ... nrois).each do |k| 
    rx1,ry1,rx2,ry2 = rois[k] 
    xo = rx1 
    yo = ry1 
    width = rx2 - rx1 
    height = ry2 - ry1

    roi_data = Array.new(height) do |r| 
      Array.new(width) do |c| 
        im[yo + r][xo + c]
      end 
    end 

    roi_im = Image.new(roi_data,im.max_value,width,height,im.colour_space,im.depth)
    roi_crop_id = "crop--roi-#{"%03d" % k}--img-#{"%03d" %i}" 
    image_name = File.basename(file_names[i],File.extname(file_names[i]))
    crop_filename = "#{dir}/#{roi_crop_id}--#{image_name}.pgm" 

    File.open(crop_filename,"w+") do |file|
      Image.write_raw(roi_im,file)
    end 

    puts crop_filename
  end 
end 
