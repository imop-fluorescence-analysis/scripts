#!/usr/bin/env crystal
#

require "./lib/image.cr"

help = <<-help 
#
# fla - fluorecense analysis
# 
# Este script se encarga de analizar la respuesta al estimulo de calcio en 
# celulas iMop, de las imágenes en formato "pnm". 
# 
# Uso: 
#   ls [imr-dir]/* | ./select-rois.rb - -n=3 | ./fla.rb - 
#   ./fla.rb - < [lista-regiones.txt]
#
# Entrada: 
#   Lee de STDIN los nombres de los archivos en orden lexicográfico en el 
#   tiempo y las regiones de interés a analizar en cada imagen
#
#   Formato: 
#     # Comentario 
#     => Nombre-archivo-1.pgm
#     x1 y1 x2 y2
#     x1 y1 x2 y2
#     => Nombre-archivo-2.pgm 
#     x1 y1 x2 y2
#     x1 y1 x2 y2
#
# Salida: 
#   Escribe a STDOUT la respuesta en luminicencia promedio normalizada de la 
#   intensidad de las regiones 
#
#   Formato: 
#     valor-roi-1 valor-roi-2 valor-roi3
#     valor-roi-1 valor-roi-2 valor-roi3
#
help

# Config  --------------------------------------------------------------------

# Parse Config ---------------------------------------------------------------
#
if ARGV.empty? || ARGV.any?{|a| a =~ /^--help$|^-h$/} || !ARGV.includes?("-") 
  puts help 
  exit 0
end 

if ARGV.any?{|a| a =~ /(--verbose|-v)/}
  verbose = true 
end 

# Programa ===================================================================

# Se asume que se pasa mediante STDIN el nombre de cada imagen a procesar y 
# las regiones de interés. 
img_rois   = [] of Array(Array(Int32))
file_names = [] of String
rois       = [] of Array(Int32)
STDIN.each_line do |line| 
  next if line =~ /(^\s*#.*$|^\s*$)/
  if line =~ /^\s*=>/
    filename = line.split(/=>\s*/)[1].strip
    file_names.push  filename   
    unless rois.empty?
      img_rois.push rois
    end 
    rois = [] of Array(Int32)
  else 
    rois.push line.strip.split(/\s+/).map{|e| e.to_f.to_i} 
  end 
end 
img_rois.push rois unless rois.empty?
exit 0 if rois.empty?

sorted_indexes = (0 ... file_names.size).to_a.sort_by{|i| file_names[i]}
img_rois       = sorted_indexes.map{|i| img_rois[i]}
file_names     = sorted_indexes.map{|i| file_names[i]}

nrois = img_rois[0].size 
nimgs = file_names.size 


# ---------------------------
def in_roi(roi,r,c) 
  x1,y1,x2,y2 = roi 

  # return (c > x1) && (c < x2) && (r > y1 ) && (r < y2)
  #
  a = ((x2 - x1)/2.0) 
  b = ((y2 - y1)/2.0) 
  cx = x1 + a
  cy = y1 + b
  a **= 2.0
  b **= 2.0
  x = (c - cx)**2.0
  y = (r - cy)**2.0

  ((x/a) + (y/b)) <= 1
end 

# Obtener la intensidad promedio de cada region ---------------------------------

rois_avg = Array.new(nrois){Array.new(nimgs,0.0)} 
(0 ... nimgs).each do |i| 
  rois = img_rois[i] 

  puts "# Loading image #{File.basename file_names[i]}"  if verbose
  im = Image.load(file_names[i])

  # Suma de los pixeles de cada roi en cada imagen
  sum = Array.new(nrois,0)
  # Cantidad de pixeles en cada roi
  pxl_cnt = Array.new(nrois,0)
  
  # Se procesa la imagen 
  (0 ... im.height).each do |r| 
    (0 ... im.width).each do |c|
      # Para cada pixel (r,c) se procesan las regiones
      (0 ... rois.size).each do |k|
        roi = rois[k]
        if in_roi(roi,r,c) 
          sum[k] += im[r][c][0]
          pxl_cnt[k] += 1
        end 
      end 
    end 
  end 
 
  # Se promedian los valores de intensidad de las rois
  (0 ... nrois).each do |k|
    rois_avg[k][i] = sum[k].to_f/pxl_cnt[k].to_f
  end 
end 

# Procesar los resultados ---------------------------------------

# Se generan series de tiempo de la diferencia de la intensidad normalizada
# con frente al tiempo 0. 

rois_lum = Array.new(nrois){[] of Float64} 
(0 ... nrois).each do |k| 
  # Calcular la intensidad promedio de fondo
  bg_lum = rois_avg[k][0]
 
  # Calcular el cambio en la intensidad con respecto al estado basal 
  delta_lum = rois_avg[k].map{|e| e - bg_lum} 

  # Normalizar con respecto a la intensidad de fondo 
  delta_lum = delta_lum.map{|e| e / {1,bg_lum.to_f}.max} 

  rois_lum[k] = delta_lum
end 

(0 ... nimgs).each do |i|
  (0 ... nrois).each do |k| 
    print "#{rois_lum[k][i]}"
    print " "
  end 
  puts "" 
end 


