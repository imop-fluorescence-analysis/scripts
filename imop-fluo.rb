#!/usr/bin/env ruby 

help = <<~help
#
# imop-fluo
# 
# Programa para realizar el análisis de principio a fin
# 
# Uso: 
#   ./imop-fluo.rb [OPCIONES] 
# 
# Opciones: 
#   --steps=  | -s=  acciones a realizar default 1..8
#                      1: convertir el video *merged*.avi a pgms
#                      2: recopilar archivos a procesar
#                      3: seleccionar region a analizar
#                      4: recortar la region a analizar
#                      5: seleccionar rois
#                      6: analizar las rois
#                      7: graficar el analisis
#                      8: realizar el análsisi espectral
#                      9: generar un gif de las regiones
#
#   --nrois=  | -n=  número de regiones a seleccionar 
#   --help    | -h   desplegar esta información 
#
# Entrada: 
# 	Recibe las diferentes opciones
#
#
# Salida: 
#   Al finalizar el directorio del experimento contendrá los siguientes archivos:
#   adicionales: 
#     [exp]/
#       ├─ crop/              # Directorio con los recortes de la región a analizar
#       ├─ files.txt          # Imagenes procesadas en el directorio imgs/
#       ├─ roi-to-crop.txt    # Region a analizar del experimento 
#       ├─ crop-files.txt     # Nombre de los archivos recortados
#       ├─ rois.txt           # Regiones de interés a procesar
#       ├─ rois-fluo-data.txt # Evolución de la fluorecencia de las regiones
#       └─ fluo-graph.svg     # Gráfica
# 
help


nrois = 10
steps  = 1..8

if ARGV.any?{|a| a =~ /^--help$|^-h$/}
  puts help 
  exit 0
end 

if k = ARGV.find_index{|a| a =~ /--?n(rois)?=?/}
  if ARGV[k] =~ /=/
    nrois = ARGV[k].split(/=/)[-1].to_i
  else
    nrois = ARGV[k+1].to_i 
  end 
end 
if k = ARGV.find_index{|a| a =~ /--?s(teps)?=?/}
  if ARGV[k] =~ /=/
    steps = Range.new(*ARGV[k].split(/=/)[-1].split("..").map(&:to_i))
  else
    steps = Range.new(*ARGV[k+1].split("..").map(&:to_i))
  end 
end 

if steps.include? 1
  puts "# Creando las imágenes a partir del archivo de video" 
  `mkdir -p out`
  `ffmpeg -loglevel panic -hide_banner -i $(find . -maxdepth 1 -iname "*merged*.avi" -print) intensity-%05d.pgm`
  `mkdir -p out/pgms-merged`
  `mv *.pgm out/pgms-merged/`
  `ffmpeg -loglevel panic -hide_banner -i $(find . -maxdepth 1 -iname "*rgb*.avi" -print) intensity-%05d.pgm`
  `mkdir -p out/pgms-rgb`
  `mv *.pgm out/pgms-rgb/`
end

if steps.include? 2 
  puts "# Recopilando archivos a procesar"
  `find out/pgms-merged/  -type 'f' -name '*.pgm' -print > files-merged.txt`
  `find out/pgms-rgb/ -type 'f' -name '*.pgm' -print > files-rgb.txt`
end 

if steps.include? 3 
  puts "# Seleccionar región del a imagen a analizar"
  `select-rois.exe > roi-to-crop-merged.txt < files-merged.txt`
  `sed 's/pgms-merged/pgms-rgb/g' roi-to-crop-merged.txt > roi-to-crop-rgb.txt`
end 

if steps.include? 4 
  puts "# Recortar región a analizar" 
  `crop-rois.cr --release -- --dir-name=crop-rgb - < roi-to-crop-rgb.txt > crop-files-rgb.txt`
  `crop-rois.cr --release -- --dir-name=crop-merged - < roi-to-crop-merged.txt > crop-files-merged.txt`
end 

if steps.include? 5 
  puts "# Seleccionar rois"
  `select-rois.exe  < crop-files-merged.txt > rois-merged.txt`
  `sed 's/merged/rgb/g' rois-merged.txt > rois-rgb.txt`
end 

if steps.include? 6 
  puts "# Analizar rois"
  `fla.cr --release -- - < rois-rgb.txt > rois-fluo-data.txt`
end 

if steps.include? 7 
  puts "# Graficar rois"
  `plot-rois-lum.py - < rois-fluo-data.txt > out/fluo-graph.svg`
end 

if steps.include? 8
  puts "# Análisis espectral"
  `spectral-analysis.py --peaks --sep=" " - < rois-fluo-data.txt`
end 

if steps.include? 9 
  puts "# Crear el video" 
  `make-video.cr --release -- - < rois-merged.txt`
end


puts "# Terminado" 
puts "# >^.^<"
