class Image

  BYTEFORMAT = IO::ByteFormat::BigEndian

  property :data,:max_value,:width,:height,:colour_space,:depth
  
  def initialize(
    data : Array(Array(Array(Int32))),
    max_value : Int32,
    width : Int32,
    height : Int32,
    colour_space : Symbol,
    depth : Int32) 
    @data = data
    @max_value = max_value
    @width = width 
    @height = height 
    @colour_space = colour_space
    @depth = depth 
  end 
  
  def [](value)
    @data[value]
  end 



  def self.is_pnm?(bytes)
    case String.new(bytes[0,2]).upcase
    when "P1","P2","P3","P4","P5","P6" then true 
    else false 
    end 
  end 

  # Reads the plain - ascii raster from 'file' into 
  # 'img' array
  #
  def self.load_plain(file,img) 
    img.size.times do |row|
      img[0].size.times do |col|
        img[0][0].size.times do |chnl| 
          img[row][col][chnl] = gets_element(file).to_i32
        end 
      end 
    end 
  end 

  # Reads the 'file' until a pnm plain-ascii element is read and 
  # returns it
  #
  def self.gets_element(file) 
    String.build do |str|
      reading = false
      comment = false 
      
      while(chr = file.read_char)
        if !chr.whitespace? && !reading && !comment
          reading = true
          if chr == '#'
            comment = true 
            reading  = false 
          end 
        end

        comment = false if comment && (chr == '\n')
        break           if chr.whitespace? && reading
        str << chr      if reading && !comment
      end
    end
  end 

  # Reads the raw - pnm raster from 'file' into 'img' array
  #
  def self.load_raw(file,img,mx_val) 
    if mx_val 
      type = (mx_val < 256) ? UInt8 : UInt16
      img.size.times do |row|
        img[0].size.times do |col|
          img[0][0].size.times do |chnl| 
            img[row][col][chnl] = file.read_bytes(type,BYTEFORMAT).to_i32
          end 
        end 
      end
    else 
      bits = Slice.new(img.size*((img.size+1)//sizeof(UInt8)),0u8)
      cnt = file.read(bits)
      index = 0
      img.size.times do |row|
        img[0].size.times do |col|
          byte_index,bit_index = index.divmod(8)
          bit_index = 7 - bit_index
          img[row][col][0] =  1 & (bits[byte_index] >> bit_index)
          index += 1
        end 
      end 
    end 
    img 
  end 


  # Loads an PNM image file
  #
  # The expected format is : 
  # 
  # 1. The ASCII Magic number 
  # 2. Whitespace 
  # 3. Width
  # 4. Whitespace 
  # 4. Height 
  # 5. Whitespace
  # 6. Maximun Value (color depth) unless the format is .pbm == "P1"  
  # 7. Whitespace
  # 8. The raster of the image of 'height' rows, each row consists of 'width' 
  #    pixels, each pixel is a tuple of the channels.   
  #      There are two variants of the raster format: 
  #        * First a plain-ascii format.  
  #          Specified by the Magic numbers = [P1,P2,P3,P8]
  #        * Second a raw format which consist of the raw bytes of each pixel. 
  #          Specified by the Magic numbers = [P4,P5,P6,P9]  
  #
  # Strings starting with "#" may be comments
  #
  def self.load(filename,img = nil ) 
    File.open(filename,"rb") do |file| 
      header       = gets_element(file).upcase
      colour_space = Image.parse_colour_space(header)
      width        = gets_element(file).to_i
      height       = gets_element(file).to_i
      mx_val       = gets_element(file).to_i unless colour_space == :bw
      depth        = Image.depth(colour_space)

      img = Array.new(height){ Array.new(width){ Array.new(depth,0)}}
      
      case header
        when "P1","P2","P3" then load_plain(file,img) 
        when "P4","P5","P6" then load_raw(file,img,mx_val)
      end 

      Image.new(img, mx_val || 1, width, height, colour_space,depth)
    end 
  end 

  def self.parse_colour_space(header) 
    case header 
    when "P1","P4" then :bw
    when "P2","P5" then :gs
    when "P3","P6" then :rgb
    else raise "Unknown format #{header}" 
    end 
  end 

  def self.depth(colour_space)
    case colour_space
    when :bw,:gs then 1
    when :rgb     then 3
    else raise "wrong colour_space #{colour_space}" 
    end
  end 




  def self.write_raw(img,io = STDOUT) 
    csp = case img.colour_space
    when :bw   then "P4" 
    when :gs   then "P5" 
    when :rgb  then "P6" 
    end 

    io << csp << "\n"
    io << img.width << " " << img.height << "\n" 

    if !(img.colour_space == :bw)  
      io << img.max_value << "\n"
      single_byte = (img.max_value < 256)
      img.height.times do |r|
        img.width.times do |c|
          img[r][c].size.times do |d|
            value = img[r][c][d]
            value = single_byte  ? value.to_u8: value.to_u16
            io.write_bytes(value,BYTEFORMAT)
          end
        end 
      end
    else 
      bits = Slice.new(img.height*((img.width+1)//sizeof(UInt8)),0u8)
      index = 0
      img.height.times do |r|
        img.width.times do |c|
          byte_index,bit_index = index.divmod(8) 
          bit_index = 7 - bit_index
          bits[byte_index] |= (1 & img[r][c][0]) << bit_index 
          index += 1
        end 
      end 
      bits.each do |byte| 
        io.write bits
      end 
    end 
  end 

  # Plain ASCII export of the image into io
  # 
  # The exported format is : 
  # 
  # 1. The ASCII Magic number 
  # 2. Whitespace 
  # 3. Width
  # 4. Whitespace 
  # 4. Height 
  # 5. Whitespace
  # 6. Maximun Value (color depth) unless is a black and white ".pbm" format
  # 7. Whitespace
  # 8. The raster of the image of height rows, each row consists of 'width' 
  #    pixels. Each pixels is a tuple of the channels and all values (channels
  #    and pixels are separated by whitespace. 
  #
  def self.write(img,io = STDOUT)
    case img.colour_space  
    when :bw   then io << "P1" << "\n"
    when :gs   then io << "P2" << "\n"
    when :rgb  then io << "P3" << "\n"
    end 
    io << img.width << " " << img.height << "\n"
    io << img.max_value << "\n" unless img.colour_space == :bw
  
    img.height.times do |r|
      img.width.times do |c|
        img[r][c].size.times do |d|
          io << img[r][c][d]
          io << " " if d < (img[r][c].size - 1) 
        end
        io << " " if c < (img.width - 1)
      end 
      io << "\n"
    end
  end  
end 
