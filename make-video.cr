#!/usr/bin/env crystal 

help = <<-HELP
#
# make-video
# 
# Este programa se encarga de generar una animación de las regiones de interés
#
# Uso:
#   ./make-video.cr -- - < [regiones.txt] 
#
# Opciones: 
#   --out-dir=[out]    ruta donde colocar el directoriod e imágenes del video 
#   --dir-name=[video] directorio donde se colocan las imágenes
#   --verbose | -v     
#   --help    | -h 
#
# Entrada: 
#
#   Lee de STDIN los nombres de las imágenes y sus rois correspondientes 
#   
#   Formato: 
#     => nombre-archivo-01.pgm 
#     x1 y1 x2 y2 
#     x1 y1 x2 y2
#     => nombre-archivo-02.pgm 
#     x1 y1 x2 y2 
#     x1 y1 x2 y2 
#
#
# Salida: 
#   Genera archivos svg con las regiones y una animación en los directorios
#   especificados. 
#
HELP

# Config -----------------------------------------------------------------------
#
verbose   = true
out_dir   = "out"
dir_name  = "video"
video_dir = "#{out_dir}/#{dir_name}"

# Config Parsing --------------------------------------------------------------
#
if ARGV.empty? || ARGV.any?{|a| a =~ /^--help$|^-h$/} || !ARGV.includes?("-") 
  puts help 
  exit 0
end 

if k = ARGV.index{|a| a =~ /--out-dir=?/}
  out_dir = (ARGV[k] =~ /=/) ? ARGV[k].split(/=/)[-1] : ARGV[k+1]
end 

if k = ARGV.index{|a| a =~ /--dir-name=?/}
  dir_name = (ARGV[k] =~ /=/) ? ARGV[k].split(/=/)[-1] : ARGV[k+1]
end 

if k = ARGV.index{|a| a =~ /(--verbose|-v)/}
  verbose = true 
end 

video_dir = "#{out_dir}/#{dir_name}"


# PROGRAMA ========================================================================
#

COLORS = [     
  0xffff00FF_u32, # amarillo
  0xff00ffFF_u32, # magenta
  0x00ffffFF_u32, # cyan
  0xff0000FF_u32, # rojo 
  0x00ff00FF_u32, # verde
  0xbfbfbfFF_u32, # gris
  0x006300FF_u32, # verde oscuro
  0xffa300FF_u32, # naranja 
  0x7f007fFF_u32, # magenta oscuro  
  0x9e512bFF_u32, # marrón
  0xFFD700FF_u32, # gold       
  0xFF4500FF_u32, # orangered  
  0xFF7F50FF_u32, # coral      
  0x008B8BFF_u32, # darkcyan          
  0x00CED1FF_u32, # darkturquoise     
  0x48D1CCFF_u32, # mediumturquoise   
  0xAFEEEEFF_u32  # paleturquoise     
] 


struct Roi
  property x1,y1,x2,y2,color
  def initialize(
    @x1 : Int32,@y1 : Int32,
    @x2 : Int32,@y2 : Int32,
    @color : UInt32)
  end
  def center
    {(@x1 + (@x2 - @x1)/2),(@y1 + (@y2 - @y1)/2)}
  end 
  def size
    {@x2 - @x1, @y2 - @y1}
  end 
end 

# Cargar rois e imagenes
imgs_files = []  of String
imgs_rois  = Array.new(imgs_files.size){[] of Roi}

# Leer las regiones de interés y los archivos de imágenes.
rois = [] of Roi
while line = STDIN.gets 
  next if line =~ /(^#.*$|^\s*$)/ 
  if line =~ /^=>\s*/ 
    rois = [] of Roi
    filename = line.split(/\s+/)[1]
    imgs_files.push filename
    imgs_rois.push rois
  else 
    x1,y1,x2,y2 = line.strip.split(/\s+/).map{|e| e.to_f.to_i}
    rois.push Roi.new(x1,y1,x2,y2,COLORS[rois.size])
  end 
end 


img_path = imgs_files[0] 
img_rois  = imgs_rois[0] 

def svg_image(img_path,img_rois,io) 
  image_path =  ARGV[0] 
  w,h =  `identify -format "%[w]x%[h]" #{img_path}`.split('x').map(&.to_f.to_i)
  svg_w,svg_h = w,h

  io.puts  <<-SVG 
  <?xml version="1.0"?>
  <svg version="1.1" 
    width="#{svg_w}" height="#{svg_h}" 
    viewBox="0 0 #{w} #{h}"
    xmlns="http://www.w3.org/2000/svg"
    xmlns:xlink="http://www.w3.org/1999/xlink">
  SVG

  io.puts <<-SVG
  <rect 
    x="0" y="0"
    width="#{svg_w}"
    height="#{svg_h}"
    fill="#000000">
  </rect>

  <image x="0" y="0" width="#{w}" height="#{h}" 
    xlink:href="data:image/png;base64,#{`convert #{img_path} PNG:- | base64`}">
  </image>
  SVG

  img_rois.each do |roi| 
    cx,cy = roi.center
    rx,ry = roi.size.map(&./(2))
    color = "%06s" % (roi.color >> 8).to_s(16,true)
    io.puts <<-SVG
    <ellipse
      cx="#{cx}" cy="#{cy}" rx="#{rx}" ry="#{ry}" 
      stroke="##{color}" stroke-width="2"
      fill="none"
    ></ellipse> 
    SVG
  end 

  io.puts <<-SVG 
  </svg>
  SVG
end 

Dir.mkdir(video_dir) unless Dir.exists?(video_dir)
(0 ... imgs_files.size).each do |i|
  img_path = imgs_files[i] 
  puts "# drawing image #{File.basename img_path}" if verbose
  File.open("#{video_dir}/rois--video--#{"%05d" % i}.svg","w+") do |file|
    svg_image(img_path,imgs_rois[i],file)
  end 
end 

puts "# mergin video"  if verbose
`convert -delay 0.5 -loop 0 #{video_dir}/*.svg #{video_dir}/video.gif`
puts "# done"  if verbose 


