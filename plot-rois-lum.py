#!/usr/bin/env python3

import sys 
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import fileinput 


colors = [
    "#AFEEEE", # paleturquoise     
    "#48D1CC", # mediumturquoise   
    "#00CED1", # darkturquoise     
    "#008B8B", # darkcyan          
    "#FF7F50", # coral      
    "#FF4500", # orangered  
    "#FFD700", # gold       
    #------------------
    "#9e512b", # marrn
    "#7f007f", # magenta oscuro  
    "#ffa300", # naranja 
    "#006300", # verde oscuro
    "#bfbfbf", # gris
    "#00ff00", # verde
    "#ff0000", # rojo 
    "#00ffff", # cyan
    "#ff00ff", # magenta
    "#ffff00" # amarillo
]
colors.reverse()

help = '''
# plot-rois-lum
#
#   Este programa se encarga de graficar las regiones
#
# Uso : 
#   ./fla.rb - < regiones.txt | ./plot-rois-lum.py - 
#   ./plot-rois-lum.py - < [analisis-de-evolución.txt]
#
# Entrada: 
#   Recibe a SDTIN la respuesta de cada región 
#   
#   Formato: 
#       valor-roi-1 valor-roi-2 valor-roi-3
#       valor-roi-1 valor-roi-2 valor-roi-3
#       valor-roi-1 valor-roi-2 valor-roi-3
#
# Salida: 
#   Muestra la gráfica y escribe un archivo en formato "png"
#
'''.strip()

if ("-" not in sys.argv): 
    print(help)
    sys.exit(0)

data = [] 
for line in fileinput.input(): 
    sample = line.strip().split(' ')
    measure = []
    for s in sample: 
        measure.append(float(s))
    data.append(measure)

rois = [[0 for x in range(0,len(data))] for y in range(0,len(data[0]))] 
for t in range(0,len(rois)):
    for v in range(0,len(rois[0])):
        rois[t][v] = data[v][t]


labels = [ "roi-"+str(i) for i in range(0,len(rois)) ]
colors = colors[0:len(rois)]

fig = plt.figure() 
ax = plt.subplot(111)

# Data for plotting
t = [i for i in range(1,len(rois[0])+1)]

for i in range(0,len(rois)): 
    ax.plot(t, rois[i],color = colors[i])

bx = ax.get_position()
ax.set_position([bx.x0,bx.y0,bx.width*0.9,bx.height])

ax.legend(labels,loc='center left',bbox_to_anchor=(1,0.5)) 
plt.savefig(sys.stdout.buffer,format="svg",transparent=True,dpi=900)
# plt.show()
