#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h> 
#include <string.h>
#include <stdbool.h>
#include <math.h>

// Definiciones ===============================================================

// Roi ------------------------------------------------------------------------

// Formas para la visualización de la región
typedef enum {ellipse,rect}Roi_shape;

// Estructura de la región 
typedef struct{ 
  int x1;             // posición x de la esquina superior izquierda
  int y1;             // posición y de la esquina superior izquierda
  int x2;             // posición x de la esquina inferior derecha 
  int y2;             // posición y de la esquina inferior derecha
  unsigned int color; // color de la región 
  bool active;        // bandera que indica si la región esta activa
  Roi_shape shape;    // forma de visualización de la región
}Roi; 

// Inicialización de la región 
void init_roi(Roi *roi);  

// Almacena el ancho y alto de la región en las variables width y height
int roi_size(Roi *roi,int *w, int *h); 

// Obtiene el centro de la roi y lo alamcena en las variables cx, y cy.
int roi_center(Roi *roi, int *cx, int *cy); 

// Regresa true si el punto mx,my se encuentra dentro de la región de 
// interes roi o false en caso contrario 
bool check_inroi(Roi *roi,int mx,int my); 

// Actualiza la posición de la región roi con paso dx,dy
void update_roi_pos(Roi *roi,int dx, int dy); 

// Cambia las dimensiones de la región roi en dx,dy y utiliza la posición 
// mx,my para saber que esquina actualizar
void scale_roi(Roi *roi,int mx, int my, int dx, int dy); 

// Cambia la forma que se dibuja en la región 
void toggle_roi_shape(Roi *roi); 

// Programa -------------------------------------------------------------------

// Modos del programa 
typedef enum {
  none,       // No se realiza ninguna acción solo se ven las imágenes
  toggle,     // Se cambia la visualización de la región
  move,       // Se mueve la región seleccionada
  scale,      // Se cambia el tamaño de la región seleccionada 
  add_roi,    // Se agrega una nueva región 
  add_scale,  // Se cambia el tamaño de la nueva región 
  delete,     // Se elimina la región seleccionada
  next,       // Se muestra la siguiente imagen 
  prev,       // Se muestra la imagen anterior 
  ignore,     // Se elimina la imagen
  help        // Se muestra la ayuda
}Mode; 

// Extraer los valores red, green, blue, y alpha de un entero de 32 bits
void rgba_from_color(unsigned int color, int *r, int *g, int *b ,int *a); 

// Dibuja una ellipse en el renderer con centro cx,cy, diagonales rx,ry, 
// tamaño de linea lw y color color;
void draw_ellipse(SDL_Renderer *renderer,
    int cx, int cy, int rx, int ry,int line_width,unsigned int color); 

// Dibuja un rectangulo en el renderer con esquina superior izquierda x1,y1
// esquina inferior derecha x2,y2, ancho de linea line_width y color color; 
void draw_rect(SDL_Renderer *renderer,
    int x1, int y1, int x2, int y2,int line_width,unsigned int color); 

// Dibuja la región roi en el renderer 
void draw_roi(SDL_Renderer * renderer,Roi *roi);
 
// Lee los nombres de los archivos de stdin y los almacena en el arreglo 
// filenames, almacena en size la cantidad de líneas. 
void read_file_names_from_stdin(char **file_names,int *size); 

// Actualiza la imagen en la textura texture con la imagen del archivo 
// en fname.
void change_image(SDL_Texture **texture,SDL_Renderer *renderer,char *fname); 

// Dibuja la barra de progreso 
void draw_progress_bar(SDL_Renderer *renderer,
  double progress,int x,int y, int h, int w,unsigned int color);

// Actualiza la escala sx,sy (imagen/ventana) 
void update_scale( SDL_Renderer *renderer, SDL_Texture *texture, 
    SDL_Window * window, double *sx, double *sy); 

// Agrega una nueva región de interés , en la posición mx, my, en todas las 
// imágenes (imgs_rois) en la posición nrois, y le asigna el color especificado
// por su posición en colors.
void new_roi( Roi **imgs_rois, int nimgs, int nrois, int mx, int my,
    unsigned int *COLORS); 

// Elimina una región de interés en la posición index, de todas las imágenes 
// (imgs_rois), al eliminarse actualiza los colores de las regiones siguientes. 
void delete_roi(Roi **imgs_rois,int nimgs,int nrois,int index); 

// Ordena en orden lexicográfico las líneas en el arreglo arr 
void sort_lines(char* arr[], int n); 

// Genera la caja de ayuda, despliega toda la ayuda si la variable full es 
// true o solo un mensaje "h-ayuda" si es false
void draw_help(SDL_Renderer * renderer,bool full); 


// Descarta una imagen, borrando de imgs_fnames el archivo, de imgs_rois 
// las regiones correspondientes a esa imagen.
void delete_image(
  char **imgs_fnames, Roi ** imgs_rois, int img_ptr, int nimgs); 

// Dibuja texto text, en renderer, en la posición x,y con tamaño font_size 
// y color color; 
void draw_text( SDL_Renderer *renderer, char * text, int x, int y, 
    int font_size, unsigned int color); 
 


// Programa principal ==========================================================

int main(int argc, char* args[]){ 

  // Inicialización -----------------------------------------------------------
  const int MAX_ROIS    = 18;   // regiones máximas 
  const int MAX_IMAGES  = 1000; // máxima capacidad de imágenes
  const int W_WIDTH     = 512;  // Ancho de la ventana 
  const int W_HEIGHT    = 512;  // Alto de la ventana 
  // Colores de las regiones
  unsigned int COLORS[] = {     
    0xffff00FF, // amarillo
    0xff00ffFF, // magenta
    0x00ffffFF, // cyan
    0xff0000FF, // rojo 
    0x00ff00FF, // verde
    0xbfbfbfFF, // gris
    0x006300FF, // verde oscuro
    0xffa300FF, // naranja 
    0x7f007fFF, // magenta oscuro  
    0x9e512bFF, // marrón
    0xFFD700FF, // gold       
    0xFF4500FF, // orangered  
    0xFF7F50FF, // coral      
    0x008B8BFF, // darkcyan          
    0x00CED1FF, // darkturquoise     
    0x48D1CCFF, // mediumturquoise   
    0xAFEEEEFF  // paleturquoise     
  }; 

  // Variable que determina si se sale del programa
  bool quit  = false;
  // Variable para manejar los eventos de drag 
  bool click = false;
  // Posiciones del raton 
  int dx,dy,mx,my,mx_old,my_old; 
  dx = dy = mx = my = mx_old,my_old = 0; 

  // Ficheros de imágenes ---------------------------------------------------- 
  int nimgs = 0; 
  char **imgs_fnames = malloc(MAX_IMAGES*sizeof(char*)); 
  for(int i=0; i < MAX_IMAGES; i+=1){imgs_fnames[i] = NULL;} 
  read_file_names_from_stdin(imgs_fnames,&nimgs);
  sort_lines(imgs_fnames,nimgs);

  // Generar las regiones 
  Roi ** imgs_rois = malloc(nimgs*sizeof(Roi*));
  for(int i=0; i < nimgs; i +=1){ 
    imgs_rois[i] = malloc(MAX_ROIS*sizeof(Roi)); 
    for(int k=0; k < MAX_ROIS; k+=1){ 
      init_roi(imgs_rois[i]+k); 
    } 
  } 

  // Apuntador de la imagen actual 
  int img_ptr = 0;
  // Apuntador a las regiones de la imagen actual 
  Roi * img_rois = imgs_rois[img_ptr];
  // Número de regiones 
  int nrois = 0; 
  // Región actual 
  Roi * roi = NULL; 


  // Inicializar SDL ----------------------------------------------------------
  SDL_Init(SDL_INIT_VIDEO); 
  IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG); 
  TTF_Init(); 

 
  // Crear la ventana --------------------------------------------------------
  SDL_Window* window = SDL_CreateWindow("select-rois",
      SDL_WINDOWPOS_CENTERED,
      SDL_WINDOWPOS_CENTERED,
      W_WIDTH,
      W_HEIGHT,
      SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

  // Crear los objetos para dibujar -------------------------------------------
  SDL_Renderer *renderer = SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED);
  SDL_Surface  *image    = IMG_Load(imgs_fnames[img_ptr]);
  SDL_Texture  *texture  = SDL_CreateTextureFromSurface(renderer,image);
	SDL_FreeSurface(image);

  
  // Escala de la ventana 
  double sx,sy; 
  update_scale(renderer,texture,window,&sx,&sy);   
  // Modo inicial de la aplicación
  Mode mode = none; 
  // Evento 
  SDL_Event event; 

  // Loop principal ------------------------------------------------------------

  while (!quit){ 

    // Esperamos un evento 
    SDL_WaitEvent(&event); 
    dx = dy = 0;  
    // Dependiendo del tipo de evento se realiza una acción 
    switch(event.type){ 

      // Cerrar la aplicación 
      case SDL_QUIT : { quit = true; break; } 

      // Se presiona una tecla 
      case SDL_KEYDOWN : { 
        // Tipo de tecla 
        switch(event.key.keysym.sym){ 
          case SDLK_q : { quit = true; break;} 
          case SDLK_t : { mode = toggle; break;} 
          case SDLK_m : { mode = move; break; }
          case SDLK_r : { if(!click){ mode = add_roi;} break; }
          case SDLK_s : { mode = scale; break; }
          case SDLK_n : { mode = next; break; }
          case SDLK_p : { mode = prev; break; } 
          case SDLK_d : { mode = delete; break; } 
          case SDLK_x : { mode = ignore; break; } 
          case SDLK_h : { 
            if(mode == help){mode = none;} 
            else{ mode = help; }
            break;
          } 
        } 
        break;
      }

      // Se presiona un boton del mouse, se inicia los eventos de drag
      case SDL_MOUSEBUTTONDOWN : { click = true; break; } 
      // El raton se mueve, se actualiza su posición en coordenadas de la 
      // imagen 

      case SDL_MOUSEMOTION : {
        mx_old = mx; 
        my_old = my; 
        SDL_GetMouseState(&mx,&my);  
        mx *= sx; 
        my *= sy; 
        dx = mx - mx_old; 
        dy = my - my_old; 
        break; 
      } 
      // Se libera el boton del mouse, se termina el evento drag
      case SDL_MOUSEBUTTONUP : { click = false; mode = none; break; } 
      
      // Se redimensiona la ventana 
      case SDL_WINDOWEVENT : { 
        update_scale(renderer,texture,window,&sx,&sy);   
        break; 
      } 
    }

    // Borramos lo dibujado anteriormente 
    SDL_RenderClear(renderer); 

    // Cambiar imagen ---------------------------------------------------------

    if (mode == ignore){ 
      if (nimgs > 1) { 
        delete_image(imgs_fnames,imgs_rois,img_ptr,nimgs); 
        nimgs -= 1; 
        img_ptr = (img_ptr < nimgs) ? img_ptr : nimgs - 1; 
        img_ptr = (img_ptr >= 0 ) ? img_ptr : 0; 
        img_rois = imgs_rois[img_ptr]; 
        change_image(&texture,renderer,imgs_fnames[img_ptr]); 
        update_scale(renderer,texture,window,&sx,&sy);   
      } 
      mode = none; 
    } 

    if(mode == next){ 
      img_ptr += 1;
      img_ptr = (img_ptr < nimgs) ? img_ptr : nimgs - 1;
      change_image(&texture,renderer,imgs_fnames[img_ptr]);
      update_scale(renderer,texture,window,&sx,&sy);   
      img_rois = imgs_rois[img_ptr];
      mode = none; 
    } 

    if(mode == prev){ 
      img_ptr -= 1; 
      img_ptr = (img_ptr >= 0) ? img_ptr : 0;
      change_image(&texture,renderer,imgs_fnames[img_ptr]);
      update_scale(renderer,texture,window,&sx,&sy);   
      img_rois = imgs_rois[img_ptr];
      mode = none; 
    } 
    
    // Dibujar imagen ----------------------------------------------------------
    
    // Dibujar la imagen 
    SDL_RenderCopy(renderer,texture,NULL,NULL); 
    
    // Dibujar la ayuda 
    if(mode == help){ 
      draw_help(renderer,true); 
    }else{draw_help(renderer,false);} 

    // Dibujar la barra de progreso 
    int tw, th; 
    SDL_QueryTexture(texture,NULL,NULL,&tw,&th);   
    draw_progress_bar(renderer,(double)img_ptr/(nimgs-1),0,th-20,tw,20,0x00FFFF80);
    char progress_text[100];
    sprintf(progress_text,"%04d : %04d\0",img_ptr,nimgs); 
    draw_text(renderer,progress_text,tw-strlen(progress_text)*9,th-15,12,0x00FFFFFF); 

    // Acciones de región  -----------------------------------------------

    // Encontrar la región activa 
    int active_index = -1; 
    for (int i=0; i < nrois; i += 1){ 
      Roi *r = img_rois + i; 
      if (check_inroi(r,mx,my)){active_index = i; }
      r->active = false; 
    }
   
    // Activamos la región 
    roi = NULL; 
    if (active_index >= 0 && active_index < MAX_ROIS){ 
      roi = img_rois + active_index; 
      roi->active = true; 
    } 

    // Se agrega una nueva región 
    if(mode == add_roi && click ){
      if (nrois < MAX_ROIS){ 
        new_roi(imgs_rois,nimgs,nrois,mx,my,COLORS); 
        roi = img_rois + nrois;
        active_index = nrois; 
        nrois += 1; 
      } 
      mode = add_scale;
    }

    // Acciones sobre la región activa 
    if(roi != NULL){ 
      switch(mode){ 
        case toggle : { 
          for(int i=0; i < nimgs; i+=1){ 
            Roi *roi = imgs_rois[i] + active_index; 
            toggle_roi_shape(roi); 
          } 
          mode = none;
          break; 
        } 
        case move: {
          if(click){
            for(int i=img_ptr; i < nimgs; i+= 1){
              Roi *roi = imgs_rois[i] + active_index; 
              update_roi_pos(roi,dx,dy);
            }
          }  
          break; 
        }
        case scale: {
          if(click){
            for(int i=img_ptr; i < nimgs; i+= 1){ 
              Roi *roi = imgs_rois[i] + active_index;
              scale_roi(roi,mx,my,dx,dy);
            } 
          }
          break;
        } 
        case add_scale: { 
          if(click){
            for(int i=0; i < nimgs; i+= 1){ 
              Roi *roi = imgs_rois[i] + active_index;
              scale_roi(roi,mx,my,dx,dy);
            } 
          }
          break;
        } 
        case delete : { 
          if (nrois > 0){ 
            delete_roi(imgs_rois,nimgs,nrois,active_index); 
            nrois -= 1;
          }
          mode = none; 
          break; 
        } 
      } 
    }
    // Dibujar las regiónes
    for(int i=0; i < nrois; i += 1){ draw_roi(renderer,img_rois+i);} 
  
    // Mostrar la imagen generada ---------------------------------------------
    SDL_SetRenderDrawColor(renderer,0,0,0,0); 
    SDL_RenderPresent(renderer); 
  } 

  // Escribir a STDOUT las regiones -------------------------------------------

  for(int i=0; i < nimgs; i+=1){
    printf("=>  %s\n",imgs_fnames[i]);
    for(int k=0; k < nrois; k+= 1){ 
      Roi roi = imgs_rois[i][k]; 
      printf("%d %d %d %d\n",roi.x1,roi.y1,roi.x2,roi.y2); 
    } 
  }

  // Liberar recursos ---------------------------------------------------------

	SDL_DestroyTexture(texture);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);

  for(int i=0; i< nimgs; i+= 1){ 
    free(imgs_fnames[i]); 
    free(imgs_rois[i]);
  }
  free(imgs_fnames); 
  free(imgs_rois); 

  TTF_Quit(); 
	IMG_Quit();
	SDL_Quit();
  return 0; 
} 

// Implementación Funciones ===================================================

// Roi ------------------------------------------------------------------------

void init_roi(Roi *roi){
  roi->x1 = roi->y1 = roi->x2 = roi->y2 = 0;
  roi->active = false; 
  roi->color = 0x000000FF;
  roi->shape = ellipse;
}

int roi_size(Roi *roi,int *width, int *height){ 
  width[0]  = abs(roi->x2 - roi->x1); 
  height[0] = abs(roi->y2 - roi->y1); 
}

int roi_center(Roi *roi, int *cx, int *cy){ 
  int w,h; 
  roi_size(roi,&w,&h);
  cx[0] = roi->x1 + (w/2); 
  cy[0] = roi->y1 + (h/2); 
}

bool check_inroi(Roi *roi,int mx,int my){ 
  bool inroi = true; 
  int th = 10; 
  int rx1 = roi->x1; 
  int ry1 = roi->y1; 
  int rx2 = roi->x2; 
  int ry2 = roi->y2;

  int cx,cy,w,h; 
  roi_size(roi,&w,&h); 
  roi_center(roi,&cx,&cy); 

  inroi &= (
    (mx >= rx1-th) && 
    (mx <= rx2+th) && 
    (my >= ry1-th) && 
    (my <= ry2+th)); 

  double x = (double) (mx - cx) / ((w/2)-th); 
  double y = (double) (my - cy) / ((h/2)-th); 

  inroi &= !(((x*x) + (y*y)) <= 1.0); 

  return inroi;  
} 

void update_roi_pos(Roi *roi,int dx, int dy){ 
  roi->x1 += dx; 
  roi->y1 += dy; 
  roi->x2 += dx; 
  roi->y2 += dy; 
}

void scale_roi(Roi *roi,int mx, int my, int dx, int dy){ 
  int w,h;
  roi_size(roi,&w,&h); 
  int midx = (roi->x1 + w/2); 
  int midy = (roi->y1 + h/2);

  if(mx <= midx){ roi->x1 += dx;}
  else{ roi->x2 += dx;} 

  if(my <= midy){ roi->y1 += dy;} 
  else{ roi->y2 += dy; }  
}

void toggle_roi_shape(Roi *roi){ 
  if (roi->shape == ellipse){ roi->shape = rect;} 
  else{ roi->shape = ellipse; } 
} 

// Programa --------------------------------------------------------------------

void rgba_from_color(unsigned int color, int *r, int *g, int *b ,int *a){ 
  r[0] = 0xFF & color >> 24;
  g[0] = 0xFF & color >> 16; 
  b[0] = 0xFF & color >> 8; 
  a[0] = 0xFF & color; 
}

void draw_ellipse(SDL_Renderer *renderer,
    int cx, int cy, int rx, int ry,int line_width,unsigned int color){ 

  int r,b,g,a; 
  rgba_from_color(color,&r,&g,&b,&a); 

  int nsteps   = 200;
  double step  = (2*3.141516) / (double)nsteps;
  double angle = 0;

  int x1 = cx + rx*cos(angle); 
  int y1 = cy + ry*sin(angle); 
  for(int i=1; i <= nsteps; i+= 1){ 
    angle += step; 

    int x2 = cx + rx*cos(angle); 
    int y2 = cy + ry*sin(angle); 
  
    thickLineRGBA(renderer,x1,y1,x2,y2,line_width,r,g,b,a); 

    x1 = x2;
    y1 = y2;
  }     
} 

void draw_rect(SDL_Renderer *renderer,
    int x1, int y1, int x2, int y2,int line_width,unsigned int color){
   int r,b,g,a; 
   rgba_from_color(color,&r,&g,&b,&a); 

   thickLineRGBA(renderer,x1,y1,x2,y1,line_width,r,g,b,a); 
   thickLineRGBA(renderer,x2,y1,x2,y2,line_width,r,g,b,a); 
   thickLineRGBA(renderer,x2,y2,x1,y2,line_width,r,g,b,a); 
   thickLineRGBA(renderer,x1,y2,x1,y1,line_width,r,g,b,a); 
} 

void draw_roi(SDL_Renderer * renderer,Roi *roi){ 
  int lw = roi->active ? 2 : 1;
  switch (roi->shape){ 
    case ellipse : {
      int cx,cy,w,h; 
      roi_size(roi,&w,&h); 
      roi_center(roi,&cx,&cy); 
      draw_ellipse(renderer,cx,cy,w/2,h/2,lw,roi->color);
      break; }
    case rect : { 
      draw_rect(renderer,roi->x1,roi->y1,roi->x2,roi->y2,lw,roi->color);
      break;} 
  } 
}

void read_file_names_from_stdin(char **file_names,int *size){ 
  size_t buffsize = 4080;
  char * buffer = malloc(buffsize*sizeof(char)); 
  size[0] = 0; 
  while((fgets(buffer,buffsize,stdin)!=NULL)){ 
    int characters = 0;
    for(int i=0; i < buffsize; i+= 1){ 
      if (buffer[i] == '\n'){ break;} 
      characters += 1; 
    } 

    file_names[size[0]] = (char*) malloc((characters+1)*sizeof(char));
    int c = 0; 
    while(buffer[c] != '\n'){ 
      file_names[size[0]][c] = buffer[c]; 
      c+= 1; 
    } 
    file_names[size[0]][c] = '\0'; 
    size[0] += 1; 
  } 

  free(buffer); 
}

void change_image(SDL_Texture **texture,SDL_Renderer *renderer,char *fname){ 
	  SDL_DestroyTexture(texture[0]);
    SDL_Surface * image = IMG_Load(fname);
    texture[0] = SDL_CreateTextureFromSurface(renderer,image); 
    SDL_FreeSurface(image);
}

void draw_progress_bar(SDL_Renderer *renderer,
  double progress,int x,int y, int w, int h,unsigned int color){ 
 
  int r,g,b,a; 
  rgba_from_color(color,&r,&g,&b,&a); 

  int x2 = x + w; 
  int y2 = y + h; 
  
  rectangleRGBA(renderer,x,y,x2,y2,r,g,b,a); 
  int p = (x2 * progress); 
  boxRGBA(renderer,x,y,p,y2,r,g,b,a); 
} 

void update_scale(
    SDL_Renderer *renderer,
    SDL_Texture *texture, 
    SDL_Window * window,
    double *sx, double *sy){ 

  int tw, th; 
  SDL_QueryTexture(texture,NULL,NULL,&tw,&th);   
  sx[0] = (double) tw / SDL_GetWindowSurface(window)->w; 
  sy[0] = (double) th / SDL_GetWindowSurface(window)->h;
  SDL_RenderSetScale(renderer,1/sx[0] ,1 / sy[0]);
} 

void new_roi(
    Roi **imgs_rois,
    int nimgs,
    int nrois,
    int mx,
    int my,
    unsigned int *COLORS){ 

  for(int i=0; i < nimgs; i+= 1){ 
    Roi * roi =  imgs_rois[i] + nrois; 
    roi->x1 = mx-5; 
    roi->y1 = my-5; 
    roi->x2 = mx+10; 
    roi->y2 = my+10;
    roi->shape = ellipse;
    roi->color = COLORS[nrois]; 
  } 
} 

void delete_roi(Roi **imgs_rois,int nimgs,int nrois,int index) { 
  for(int i=0; i < nimgs; i+=1){ 
    unsigned int color = imgs_rois[i][index].color; 
    for(int k=index+1; k < nrois; k += 1){
      imgs_rois[i][k-1] = imgs_rois[i][k];
      imgs_rois[i][k-1].color = color; 
      color = imgs_rois[i][k].color; 
    } 
  } 
}

void sort_lines(char* arr[], int n){ 
  int linecomp(const void* a, const void* b){ 
    return strcmp(*(const char**)a, *(const char**)b); 
  } 
  qsort(arr,n,sizeof(const char*), linecomp); 
} 

void delete_image(
  char **imgs_fnames, Roi ** imgs_rois, int img_ptr, int nimgs){ 
  
  char *fname = imgs_fnames[img_ptr]; 
  Roi  *rois  = imgs_rois[img_ptr]; 

  for(int i=img_ptr+1; i < nimgs; i+= 1){ 
    imgs_fnames[i-1] = imgs_fnames[i]; 
    imgs_rois[i-1] = imgs_rois[i]; 
  } 

  imgs_rois[nimgs-1] = rois;
  imgs_fnames[nimgs-1] = fname; 
  free(fname);
  free(rois); 
}

void draw_help(SDL_Renderer * renderer,bool full){ 
  TTF_Font * font = TTF_OpenFont(
      "/usr/share/fonts/TTF/DejaVuSansMono.ttf",14); 

  SDL_Color color = { 0, 255, 255}; 
  char *keys[14] = { 
      "h - ayuda     ",
      "              ",
      "r - crear     ",
      "d - borrar    ",
      "              ",
      "m - mover     ",
      "s - escalar   ",
      "t - forma     ",
      "              ",
      "n - siguiente ",
      "p - anterior  ",
      "x - descartar ",
      "              ",
      "q - salir     "
    };
  int size = full ? 14 : 1;  
   
  for(int i=0; i < size; i+= 1){  
    SDL_Surface * surface = TTF_RenderText_Solid(font,keys[i],color);
    SDL_Texture * txt_texture = SDL_CreateTextureFromSurface(renderer,surface);
    SDL_Rect rect = {0,0,0,0}; 
    SDL_QueryTexture(txt_texture,NULL,NULL,&rect.w,&rect.h);
    rect.y = (i*rect.h)-1;
    rect.h += 1;
    boxRGBA(renderer,0,1+(i*rect.h),rect.w,(i+1)*rect.h,0,0,0,0x99); 
    SDL_RenderCopy(renderer,txt_texture,NULL,&rect); 
    SDL_FreeSurface(surface); 
    SDL_DestroyTexture(txt_texture); 
  } 
  TTF_CloseFont(font);
} 

void draw_text(
    SDL_Renderer *renderer, 
    char * text,
    int x,
    int y, 
    int font_size, 
    unsigned int color){ 
  
  // Fuente a utilizar
  TTF_Font * font = TTF_OpenFont(
      "/usr/share/fonts/TTF/DejaVuSansMono.ttf",14); 
  int r,g,b,a; 
  rgba_from_color(color,&r,&g,&b,&a); 
  SDL_Color sdl_color = {r,g,b};
  SDL_Surface * surface = TTF_RenderText_Solid(font,text,sdl_color);
  SDL_Texture * txt_texture = SDL_CreateTextureFromSurface(renderer,surface);
  SDL_Rect rect = {x,y,0,0}; 
  SDL_QueryTexture(txt_texture,NULL,NULL,&rect.w,&rect.h);
  SDL_RenderCopy(renderer,txt_texture,NULL,&rect); 
  SDL_FreeSurface(surface); 
  SDL_DestroyTexture(txt_texture);

  TTF_CloseFont(font);
}
