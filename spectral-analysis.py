#!/usr/bin/env python3

import sys 
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sgnl
import fileinput 
import re

colors = [
    "#AFEEEE", # paleturquoise     
    "#48D1CC", # mediumturquoise   
    "#00CED1", # darkturquoise     
    "#008B8B", # darkcyan          
    "#FF7F50", # coral      
    "#FF4500", # orangered  
    "#FFD700", # gold       
    #------------------
    "#9e512b", # marrn
    "#7f007f", # magenta oscuro  
    "#ffa300", # naranja 
    "#006300", # verde oscuro
    "#bfbfbf", # gris
    "#00ff00", # verde
    "#ff0000", # rojo 
    "#00ffff", # cyan
    "#ff00ff", # magenta
    "#ffff00" # amarillo
]
colors.reverse()
import random 
r = lambda: random.randint(0,255)
for _ in range(0,1000):
    colors.append('#%02X%02X%02X' % (r(),r(),r())) 

help = '''
# spectral-analysis.py
#
#   Este programa se encarga de generar el análisis espectral para oscilaciones
#   de calcio (Ca⁺), descrito en el protocolo de Per Uhién [1].
#
# Uso : 
#   Escribir a stdout la imgen svg
#   ./spectral-analysis.py --svg - < [datos-regiones.txt]
#
#   Escribir los picos y su potencia relativa 
#   ./spectral-analysis.py --peaks - < [datos-regiones.txt] 
# 
#   Escribir los datos y la gráfica a stdout separados por la línea '#-svg-----"
#   ./spectral-analysis.py --peaks --svg - < [datos-regiones.txt] 
#    
#
# Flags: 
#   --sep=[,]  # separador de las columnas 
#   --peaks    # escribir los picos y su potencia relativa
#   --hist     # escribir el histograma de frequencias (freqhist)
#   --psd      # escribir la gráfica de distribución de potencia 
#   --data     # escribir la gráfica de los datos 
#   --psdhist  # escribir la gráfica del histograma de potencias
#   --freqhist # escribir la gráfica del histograma de frequencias 
#   --pth=[1]  # % de la potencia relativa de un pico para ser reportado
#   --show     # muestra las gráficas de forma interactiva con matplotlib
#
# Entrada: 
#   Recibe de SDTIN la respuesta de la región a analizar
#   
#   Formato: 
#     valor-roi-1-t0 valor-roi-2-t0 ....
#     valor-roi-1-t1 valor-roi-2-t1 ....
#     valor-roi-1-t2 valor-roi-2-t2 ....
#     ... 
#
# Salida: 
#
# Referencias: 

#   [1] Uhlén, P. (2004). Spectral analysis of calcium oscillations. Sci. STKE,
#       2004(258), pl15-pl15.
'''.strip()


# Verificar que se tienen los argumentos obligatorios
# if (("-" not in sys.argv) or (("--svg" not in sys.argv) and ("--peaks" not in sys.argv))):
if (("-" not in sys.argv)):
    print(help)
    sys.exit(0)

show_plot = False 
if ("--show" in sys.argv): 
  show_plot = True 

print_psd = False 
if ("--psd" in sys.argv): 
  print_psd = True

print_data = False
if ("--data" in sys.argv): 
  print_data = True

print_psdhist = False 
if ("--psdhist" in sys.argv): 
  print_psdhist = True

print_freqhist = False 
if ("--freqhist" in sys.argv): 
  print_freqhist = True

print_peaks = False
if ("--peaks" in sys.argv): 
  print_peaks = True

print_hist = False
if ("--hist" in sys.argv): 
  print_hist = True

pth = 5
r = re.compile("--pth=?.*$")
pth_arg = list(filter(r.match,sys.argv))
if len(pth_arg) != 0: 
  arg = sys.argv[sys.argv.index(pth_arg[0])]
  r = re.compile("^--pth=.+$")
  aux = list(filter(r.match,sys.argv))
  if len(aux) != 0: 
    pth = float(aux[0].split("=")[-1])
  else:
    pth = sys.argv[sys.argv.index(pth_arg[0])+1]
    sys.argv.remove(pth) 
    pth = float(pth)

sep = ','
r = re.compile("--sep=?.*$") 
sep_arg = list(filter(r.match,sys.argv)) 
if len(sep_arg) != 0: 
  arg = sys.argv[sys.argv.index(sep_arg[0])]
  r = re.compile("^--sep=.+$")
  aux = list(filter(r.match,sys.argv))
  if len(aux) != 0: 
    sep = aux[0].split("=")[-1]
  else:
    sep = sys.argv[sys.argv.index(sep_arg[0])+1]
    sys.argv.remove(sep) 

# MAIN ========================================================================


# STEP 1: Import the text file into the program 

# Read from stdin the data files, because the columns might have 
# different sizes we firs tread all rows
for _ in range(0,len(sys.argv)-1): 
  sys.argv.pop()

data = [] 
for line in fileinput.input(): 
  sample = line.rstrip().split(sep)
  data.append(sample)

# We create an array of len(row-0) columns, and read in each row 
# the value is present
#
rois = [[] for x in range(0,len(data[0]))]
for x in range(0,len(data)):
    for y in range(0,len(data[0])):
        val = data[x][y]
        if val != '' and val != ' ':
            rois[y].append(float(val)) 

# Configure the different subplots figures. 

if print_psd: 
  psd_fig,psd_ax = plt.subplots()
  psd_ax.set_title("Power Spectral Density (PSD)")
  psd_ax.set_ylabel("PSD")
  psd_ax.set_xlabel("Frequency [mHz]")

if print_data: 
  data_fig,data_ax = plt.subplots()
  data_ax.set_title("Fluorescence relative change in time")
  data_ax.set_ylabel("ΔF/F0")
  data_ax.set_xlabel("Time [s]")

if print_psdhist: 
  psdhist_fig,psdhist_ax = plt.subplots()
  psdhist_ax.set_title("Histogram of average PSD") 
  psdhist_ax.set_ylabel("PSD") 
  psdhist_ax.set_xlabel("Frequency [mHz]") 

if print_freqhist: 
  freqhist_fig,freqhist_ax = plt.subplots()
  freqhist_ax.set_title("Frequency histogram")
  freqhist_ax.set_ylabel("# of rois ") 
  freqhist_ax.set_xlabel("Frequency [mHz]") 


if print_peaks:
  print(f"# Most relevant peaks with {pth}% criteria")
  print("# Frequency[mHz] Relative-power[%]")

# Frequency values 
hists_data = [] 

# psd_fig,psd_ax = plt.subplots()
# procesar cada region
for roi_id in range(len(rois)): 
  roi = rois[roi_id]

  # STEP 3 Assign the time column to an array 't' and the 
  # calcium recording to analyze to 'caosc'
  t = np.array([i for i in range(0,len(roi))])
  caosc = roi

  # A filter for smoothing the data against noise is used
  #
  # b,a = sgnl.butter(3,0.25)
  # caosc = sgnl.filtfilt(b,a,caosc)

  # OPT 1 to attenuate the spectral leakage multiply the recorded data with a 
  # window function that ramps smoothly up from zero at the start and smoothly 
  # back to zero at the end. 
  #
  # hanning = 0.5 - 0.5*np.cos(t*(2*np.pi)/t[-1]); 
  #caosc = caosc*hanning

  # STEP 4 determine and substract the trend component of the experiment
  trend = np.polyfit(t,caosc,deg = 2)
  #caosc = caosc  - np.polyval(trend,t)

  # STEP 5 plot the function to verify that the polynomial degree was correctly
  # chosen 
  if print_data: 
    data_ax.plot(t,caosc,color = colors[roi_id])

  # STEP 6 compute the 1-d fourier tranform of the signal, the frequency 
  # resolution can be increased by padding the end of the time series with zeros
  N = len(caosc)
  G = np.fft.fft(caosc,n=N)

  # STEP 7 obtain the power spectral density (PSD)
  # psd = abs(np.sqrt((G*np.conj(G)))/N)
  psd = abs(G)/N
  psd = psd[0:(N//2)+1]
  psd[1:(N//2)] = 2*psd[1:(N//2)]
  psd = np.insert(psd,0,0)
  
  # STEP 8 the psd data need to be mapped onto a frequency array that shows the 
  # frequencies in the signal. 
  dt = 0.5  # Time difference between each sample
  fs = 1/dt
  f = np.array([1000*((fs*x)/N) for x in range(0,len(psd))])

  # OPT 2 Find the gradient and find the peaks an valleys of the psd 
  peaks  = (np.diff(np.sign(np.diff(psd)))<0).nonzero()[0] + 1
  valley = (np.diff(np.sign(np.diff(psd)))>0).nonzero()[0] + 1
  valley = np.insert(valley,0,0)
  valley = np.insert(valley,len(valley),len(psd))
  
  # OPT 3 calculate the relative power to determine the most dominant 
  # frequencies, this quantity tells us how much power one peak holds in 
  # relation to the total power of the spectrum 
  psd_area = np.trapz(psd,f)
  rel_power = np.array([ 0 for x in range(0,len(peaks))])
  for x in range(0,len(peaks)):
    f1 = valley[x]
    f2 = valley[x+1]
    rel_power[x] = (100*(np.trapz(psd[f1:f2],f[f1:f2])/psd_area))
  

  # Plot the psd 
  if print_psd: 
    psd_ax.plot(f,psd,color = colors[roi_id])
    l = psd_ax.plot(f[peaks[rel_power > pth]],psd[peaks[rel_power > pth]],"o",color = colors[roi_id])

  # Select most relevant peaks
  f = f[peaks[rel_power > pth]]
  psd = psd[peaks[rel_power > pth]]
  rel_power = rel_power[rel_power > pth]

  # Output the relevant peaks 
  if print_peaks:
    print(f"#-roi-{roi_id}") 
  for k in range(0,len(f)): 
    if print_peaks:
      sys.stdout.flush()
      print(f"{f[k]} {rel_power[k]}")
    hists_data.append([f[k],rel_power[k],psd[k]])


# If required we save the psd svg
if print_psd:
  sys.stdout.flush()
  psd_fig.savefig(sys.stdout.buffer,format="svg",transparent=True,dpi=900)

# If required we save the data svg
if print_data: 
  sys.stdout.flush()
  data_fig.savefig(sys.stdout.buffer,format="svg",transparent=True,dpi=900)

# We calculate two histograms
# First the  psdhist that contains the average values of psd and secondly the
# freqhist containing the number of ocurrences of a frequency in the data. 
wgt   = [ hists_data[x][2] for x in range(0,len(hists_data))]
hzt   = [ hists_data[x][0] for x in range(0,len(hists_data))]
start,end,step  = 0,250,5
bins  = [x for x in range(start,end,step)]
freqhist,freqhist_edges = np.histogram(hzt,bins)
psdhist,psdhist_edges = np.histogram(hzt,bins,weights=wgt) 
psdhist = psdhist/len(rois)

bar_width = abs(bins[1] - bins[0])
# If required we save the psdhist svg 
if print_psdhist: 
  sys.stdout.flush()
  psdhist_ax.bar(psdhist_edges[0:-1],psdhist,width=bar_width,align="edge")
  psdhist_fig.savefig(sys.stdout.buffer,format="svg",transparent=True,dpi=900)

# If required we save the freqhist svg 
if print_freqhist: 
  sys.stdout.flush()
  freqhist_ax.bar(freqhist_edges[0:-1],freqhist,width=bar_width,align="edge")
  freqhist_fig.savefig(sys.stdout.buffer,format="svg",transparent=True,dpi=900)

# If required we print the histogram
if print_hist: 
  sys.stdout.flush()
  print(f"# Histograma de frecuencias (número de veces que una region presento esa frecuencia")
  print(f"# Bin-start value")
  i = 0 
  for i in range(0,len(freqhist)): 
    print(f"{freqhist_edges[i]} {freqhist[i]/len(rois)}")

if show_plot:
  plt.show()
