C = 0 

def gaussian(mean, stddev)
  theta = 2 * Math::PI * rand(0.0..1.0)
  rho = Math.sqrt(-2 * Math.log(1 - rand(0.0...1.0)))
  scale = stddev * rho
  x = mean + scale * Math.cos(theta)
  y = mean + scale * Math.sin(theta)
  return x,y
end


z = [gaussian(0.0,5)[0]]
y = [gaussian(0.0,5)[0]]
(1...250).each do |i| 

  x= gaussian(0.0,5)[0]
  
  z.push ((i < 40) ? x : 50*Math.sin(2*3.141516*i/50) + x)
  y.push ((i < 40) ? x : 50*Math.sin(2*3.141516*i/50) + x)
  # z.push ((i <= 40 ) ? x[i] : 50*Math.sin(i/4) + x[i] )
end 

(0 ... z.size).each do |i|
  print (z[i]-z[0])/255.0,",",(y[i]-y[0])/255.0,"\n"
    # z[i],",",(z[i]-z[0])/z[0],"\n"
end 
