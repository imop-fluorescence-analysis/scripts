#!/usr/bin/env ruby 
#

require "tk" 
require "tempfile"
require_relative "./lib/roi.rb"

help = <<~help
#
# view-rois 
#
# Este programa se encarga de visualizar las regiones de interés 
#
# Uso: 
#   ls in/* | ./select-rois.rb -nrois 4 - | ./view-rois.rb - 
#   ./view-rois.rb - < [regiones.txt]
#
# Opciones: 
#   --out-dir=      ruta donde colocar el directorio de imágenes del video
#   --dir-name=     directorio donde se colocan las imágenes
#   --verbose | -v 
#   --help    | -h 
# Entrada: 
#   
#   Lee de STDIN los nombres de las imágenes y sus rois correspondientes 
#   
#   Formato: 
#     => nombre-archivo-01.pgm 
#     x1 y1 x2 y2 
#     x1 y1 x2 y2
#     => nombre-archivo-02.pgm 
#     x1 y1 x2 y2 
#     x1 y1 x2 y2 
#
help

# Config -----------------------------------------------------------------------
#
verbose   = true
out_dir   = "out"
dir_name  = "video"
video_dir = "#{out_dir}/#{dir_name}"


# Config Parsing --------------------------------------------------------------
#
if ARGV.empty? || ARGV.any?{|a| a =~ /^--help$|^-h$/} || !ARGV.include?("-") 
  puts help 
  exit 0
end 

if k = ARGV.find_index{|a| a =~ /--out-dir=?/}
  out_dir = (ARGV[k] =~ /=/) ? ARGV[k].split(/=/)[-1] : ARGV[k+1]
end 

if k = ARGV.find_index{|a| a =~ /--dir-name=?/}
  dir_name = (ARGV[k] =~ /=/) ? ARGV[k].split(/=/)[-1] : ARGV[k+1]
end 

if k = ARGV.find_index{|a| a =~ /(--verbose|-v)/}
  verbose = true 
end 

video_dir = "#{out_dir}/#{dir_name}"

# PROGRAMA ========================================================================
#

# Cargar rois e imagenes
img_files = [] 
# img_ary   = []
img_rois  = Array.new(img_files.size){[]}

while (line = STDIN.gets) =~ /(^#.*$|^\s*$)/ 
end 
filename = line.split(/\s+/)[1]
img_files.push filename
# img_ary.push(TkPhotoImage.new(file: filename))


root = TkRoot.new 
root["title"] = "rois-viewer"
root["geometry"] = "800x800"
TkGrid.columnconfigure root, 0, weight: 1
TkGrid.rowconfigure    root, 0, weight: 1

# Crear canvas para mostrar las imágenes y rois
canvas = TkCanvas.new(root)
canvas["relief"] = "s"
canvas.grid sticky: "nwes", column: 0, row: 0
canvas.width =  800
canvas.height = 800
canvas.focus


rois = [] 
while line = STDIN.gets 
  next if line =~ /(^#.*$|^\s*$)/ 
  if line =~ /^=>\s*/ 
    filename = line.split(/\s+/)[1]
    img_files.push filename
    img_rois.push rois
    rois = [] 
  else 
    x1,y1,x2,y2 = line.strip.split(/\s+/).map{|e| e.to_f}
    if img_rois.empty?
      rois.push Roi.new(canvas,x1,y1,x2,y2) 
    else 
      rois.push Roi.new(canvas,x1,y1,x2,y2,:circle,img_rois[-1][rois.size].color)
    end 
  end 
end 
img_rois.push rois

img_number = img_files.size 
rois_num = img_rois[0].size 



im = TkPhotoImage.new(file: img_files[0])
sx = (canvas.width.to_f / im.width).round(0)
sy = (canvas.height.to_f / im.height).round(0)
zoom = TkPhotoImage.new
img_canvas_obj = TkcImage.new(canvas,0,0)
img_cnt = -1 


# Escalar las rois 
img_rois.each do |rois|
  rois.each do |r|
    x1,y1,x2,y2 = r.coords
    x1 *= sx 
    x2 *= sx 
    y1 *= sy
    y2 *= sy
    r.coords(x1,y1,x2,y2)
  end
end 

draw_image = -> do 
  im = TkPhotoImage.new(file: img_files[img_cnt])
  zoom.copy(im, zoom: [sx,sy])
  img_canvas_obj.delete 
  img_canvas_obj = TkcImage.new(canvas,0,0,[image: zoom,anchor: "nw"])
  rois = img_rois[img_cnt]
  (0 ... rois_num).each do |i|
    rois[i] = rois[i].draw
  end
end 

next_image = ->() do 
  if img_cnt < img_number - 1
    img_cnt += 1
    draw_image.call
  end 
end 

prev_image = ->() do 
  if img_cnt > 0 
    img_cnt -= 1
    draw_image.call
  end 
end 


take_video = -> do 
  Dir.mkdir(video_dir) unless Dir.exist?(video_dir)
  (0 ... img_number).each do |i| 
    img_cnt = i 
    puts "# drawing" if verbose
    draw_image.call
    root.update
    puts "# taking video #{i}" if verbose
    Tempfile.create("#{i}.eps") do |file|
      canvas.postscript(file: "#{file.path}")
      system "convert #{file.path} #{video_dir}/#{"%05d" % i}.jpg"
    end 
  end 
  puts "# mergin video"  if verbose
  system "convert -delay 0.5 -loop 0 #{video_dir}/*.jpg #{video_dir}/video.gif"
  puts "# done"  if verbose 
  system "rm -f #{video_dir}/*.jpg"
  exit
end 


canvas.bind("KeyPress", 
  ->(key) do 
  case key 
  when "n"  then next_image.call
  when "p"  then prev_image.call
  when "v"  then take_video.call 
  when "q"  then Tk.exit 
  end 
end, "%A"); 


next_image.call
take_video.call 
